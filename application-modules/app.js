var app = angular.module('coreApp', ['ui.router','oc.lazyLoad','ngAnimate','mgcrea.ngStrap','infinite-scroll']);
app.factory('factoryFunctions',['$timeout',function($timeout){
    function setUniformEelements(){
        $timeout(function(){$('input[type="checkbox"]').uniform()},0);
    }
    function applySlimScroll(element,height){
        $timeout(function(){$(element).slimscroll({allowPageScroll:!0,height:height})});
    }
    return {
        setUniformEelements : setUniformEelements,
        applySlimScroll : applySlimScroll
    }
}]);
app.config(['$popoverProvider',function($popoverProvider){
    angular.extend($popoverProvider.defaults, {

        trigger: 'click',
        autoClose:true
    });
}]);

app.config(['$httpProvider', function ($httpProvider) {
    // Intercept POST requests, convert to standard form encoding
    //$httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
    //$httpProvider.defaults.headers.put["Content-Type"] = "application/x-www-form-urlencoded";
}]);
app.controller('navigation',['$rootScope','$scope',function($rootScope,$scope){
        this.updateTitle = function(title){
            $rootScope.$broadcast('updateTitle',title);
        }
    }]);
app.controller('profileController',['$http',function($http){
    $http.get('')
}])
/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});

app.controller('titleUpdater',['$scope',function($scope){
       $scope.title = 'Dashboard';
       $scope.$on('updateTitle',function(event, data){
            $scope.title = data;
        })
    }]);
app.config(['$stateProvider', '$locationProvider', '$ocLazyLoadProvider','$urlRouterProvider', function ($stateProvider, $locationProvider, $ocLazyLoadProvider,$urlRouterProvider) {

    $locationProvider.html5Mode(true); // Enable HTML 5 HISTORY API
    
    //loading dashboard as per the apptype which gets set fom server
    $urlRouterProvider.otherwise(function(){
        if(appType=='doctor')
            return '/platform/doctor-dashboard'
        else
           return '/platform/patient-dashboard'
    });
    //making sure browser history works appropriately
    $urlRouterProvider.when('/platform/messages', '/platform/messages/inbox');
    
    //inbox routes begin
    $stateProvider.state('doctor-dashboard',{
        url:'/platform/doctor-dashboard',
        templateUrl:'/application-modules/doctor-dashboard/dashboard.html',
        controller: 'dashboardCtrl',
        resolve: {
            loadCtrl: function ($ocLazyLoad) {
                return $ocLazyLoad.load('/application-modules/doctor-dashboard/controller.js')
            }
        }
    })
    .state('medication',{
        url:'/platform/medication',
        templateUrl:'/application-modules/medication/layout.html'
        /*controller: 'alertsCtrl',
        resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('/application-modules/alerts/controller.js')
            }]
        }*/
    }).state('alerts',{
            url:'/platform/alerts',
            templateUrl:'/application-modules/alerts/alerts.html',
            controller: 'alertsCtrl',
            resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('/application-modules/alerts/controller.js')
                }]
            }
        }).state('profile',{
        url:'/platform/profile',
        templateUrl:'/application-modules/doctor-profile/personal-information.html',
        controllerAs: 'controller',
        controller: 'profileController',
        resolve:{
            loadCtrl: function ($ocLazyLoad) {
                return $ocLazyLoad.load('/application-modules/doctor-profile/controller.js');
            }
        }
    })
    .state('tos-doctor',{
        url:'/platform/doctor/terms-of-service',
        templateUrl:'/application-modules/settings/views/doctor_tos.html',
        controller:'tosController',
        //controllerAs:'tosController',
        resolve:{
            loadCtrl: function ($ocLazyLoad) {
                return $ocLazyLoad.load('/application-modules/settings/controllers/tosController.js');
            }
        }
    })
    .state('doctor-disclaimer',{
        url:'/platform/doctor/disclaimer',
        templateUrl:'/application-modules/settings/views/doctorDisclaimer.html',
        controller:'disclaimerController',
        //controllerAs:'controller',
        resolve:{
            loadCtrl: function ($ocLazyLoad) {
                return $ocLazyLoad.load('/application-modules/settings/controllers/disclaimerController.js');
            }
        }
    })
    .state('doctor-privacy-policy',{
        url:'/platform/doctor/privacy-policy',
        templateUrl:'/application-modules/settings/views/privacy_policy_iCancerHealth.html',
            controller:'privacyPolicyController',
            //controllerAs:'controller',
            resolve:{
                loadCtrl: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('/application-modules/settings/controllers/privacyPolicyController.js');
                }
            }
    })
    .state('about-us-patient',{
        url:'/platform/patient/about-us',
        templateUrl:'/application-modules/settings/views/patientAbout.html',
            controller:'aboutController',
            resolve:{
                loadCtrl: function($ocLazyLoad){
                    return $ocLazyLoad.load('/application-modules/settings/controllers/aboutController.js');
                }
            }
    })
    .state('tos-patient',{
        url:'/platform/patient/terms-of-service',
        templateUrl:'/application-modules/settings/views/patient_tos.html',
            controller:'tosController',
            //controllerAs:'tosController',
            resolve:{
                loadCtrl: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('/application-modules/settings/controllers/tosController.js');
                }
            }
    })
    .state('patient-disclaimer',{
        url:'/platform/patient/disclaimer',
        templateUrl:'/application-modules/settings/views/patientDisclaimer.html',
            controller:'disclaimerController',
            controllerAs:'controller',
            resolve:{
                loadCtrl: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('/application-modules/settings/controllers/disclaimerController.js');
                }
            }
    })
    .state('patient-privacy-policy',{
        url:'/platform/patient/privacy-policy',
        templateUrl:'/application-modules/settings/views/privacy_policy_iCancerHealth.html',
            controller:'privacyPolicyController',
            //controllerAs:'controller',
            resolve:{
                loadCtrl: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('/application-modules/settings/controllers/privacyPolicyController.js');
                }
            }
    })
    .state('about-us-doctor',{
        url:'/platform/doctor/about-us',
        templateUrl:'/application-modules/settings/views/doctorAbout.html',
            controller:'aboutController',
            controllerAs:'controller',
            resolve:{
                loadCtrl: function($ocLazyLoad){
                    return $ocLazyLoad.load('/application-modules/settings/controllers/aboutController.js');
                }
            }
        })
    .state('doctor-feedback',{
        url:'/platform/doctor/feedback',
        templateUrl:'/application-modules/settings/views/patientFeedback.html',
            controller:'feedbackController',
            controllerAs:'controller',
            resolve:{
                loadCtrl: function($ocLazyLoad){
                    return $ocLazyLoad.load('/application-modules/settings/controllers/feedbackController.js');
                }
            }
        })
    .state('patient-feedback',{
            url:'/platform/patient/feedback',
            templateUrl:'/application-modules/settings/views/patientFeedback.html',
            controller:'feedbackController',
            controllerAs:'controller',
            resolve:{
                loadCtrl: function($ocLazyLoad){
                    return $ocLazyLoad.load('/application-modules/settings/controllers/feedbackController.js');
                }
            }
        })
    .state('patient-personal-details',{
        url:'/platform/profile/patient/personal-details',
        templateUrl:'/application-modules/patient-profile/personal-information.html',
            controller:'patientProfileController',
            controllerAs:'controller',
            resolve:{
                loadCtrl: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('/application-modules/patient-profile/controller.js');
                }
            }
        })
    .state('patient-medical-providers',{
        url:'/platform/profile/patient/medical-providers',
        templateUrl:'/application-modules/patient-profile/medical-providers/medical-provider.html',
        controller:'medicalProviderController',
        controllerAs:'controller',
        resolve:{
            loadCtrl: function ($ocLazyLoad) {
                return $ocLazyLoad.load('/application-modules/patient-profile/medical-providers/controller.js');
            }
        }
     })
    .state('patient-medical-history',{
            url:'/platform/profile/patient/medical-history',
            templateUrl: '/application-modules/patient-profile/medical-history/layout.html',
            controller: 'allergiesController',
            controllerAs: 'controller',
            resolve:{
                loadCtrl: function($ocLazyLoad){
                    return $ocLazyLoad.load('/application-modules/patient-profile/medical-history/controller.js');
                }
            }
        })
    .state('caregiver-management',{
        url:'/platform/profile/patient/caregiver-management',
        templateUrl:'/application-modules/patient-profile/care-giver-management.html',
            controller:'caregiverController',
            controllerAs:'controller',
            resolve:{
                loadCtrl: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('/application-modules/patient-profile/caregiverController.js');
                }
            }
        }).state('community',{
            url:'/platform/community/public-forum',
            templateUrl:'/application-modules/community/public-forum/layout.html',
            controller:'publicForumController',
            controllerAs:'controller',
            resolve:{
                loadCtrl: function($ocLazyLoad){
                    return $ocLazyLoad.load('/application-modules/community/public-forum/controller.js');
                }
            }
        }).state('community-events',{
            url:'/platform/community/events',
            templateUrl:'/application-modules/community/events/layout.html',
            controller:'eventsController',
            controllerAs:'controller',
            resolve:{
                loadCtrl: function($ocLazyLoad){
                    return $ocLazyLoad.load('/application-modules/community/events/controller.js');
                }
            }
        }).state('patient-search',{
            url:'/platform/patient-search',
            templateUrl:'/application-modules/patient-search/patientSearch.html',
            controller:'patientSearchController',
            controllerAs:'controller',
            resolve:{
                loadCtrl:function($ocLazyLoad){
                    return $ocLazyLoad.load('/application-modules/patient-search/controller.js')
                }
            }
        }).state('medical-report',{
            url:'/platform/medical-report',
            templateUrl:'/application-modules/patient-search/medical-report/medicalReport.html',
            controller:'medicalReportController',
            controllerAs:'controller',
            resolve:{
                loadCtrl:function($ocLazyLoad){
                    return $ocLazyLoad.load('/application-modules/patient-search/medical-report/controller.js')
                }
            }
        }).state('transitional-billing',{
            url:'/platform/transitional-billing',
            templateUrl:'/application-modules/patient-search/transition-billing/transitionalBilling.html',
            controller:'transBillingController',
            controllerAs:'controller',
            resolve:{
                loadCtrl:function($ocLazyLoad){
                    return $ocLazyLoad.load('/application-modules/patient-search/transition-billing/controller.js')
                }
            }
        }).state('chat',{
        url:'/platform/chat',
        templateUrl:'/application-modules/chat/chat.html',
        controller:'chatCtrl',
        resolve:{
            loadCtrl:function($ocLazyLoad){
                return $ocLazyLoad.load('/application-modules/chat/controller.js')
            }
        }
    })
    .state('resources',{
        url:"/platform/resources",
        templateUrl:"/application-modules/resources/resources.html",
        controller:"resourcesCtrl",
        resolve:{
            loadctrl:function($ocLazyLoad){
                return $ocLazyLoad.load('/application-modules/resources/controller.js')
            }
        }
    })
}]);