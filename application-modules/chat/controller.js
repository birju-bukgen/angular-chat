angular.module('coreApp').controller('chatCtrl',['factoryFunctions','$http','$scope','chatFunctions','$timeout','$rootScope','$aside',function($factoryFunctions,$http,$scope,chatFunctions,$timeout,$rootScope,$aside){
    $rootScope.$broadcast('updateTitle','Messages');
    $factoryFunctions.applySlimScroll('.chatRepliesScroll',644);
    $scope.pageThread = 1;
    $scope.noMoreData = false;
    $scope.user = {};
    $scope.user.selectedThread = {};
    $scope.user.id = localStorage.getItem('userid');
    $scope.thread = {};
    $scope.multipleChecked = [];
    chatFunctions.getThreads(1).then(function(response){
        $scope.threads = response.data.message;
        $scope.user.selectedThread.id = response.data.message[0].id;
        $scope.user.selectedThread.subject = response.data.message[0].subject;
        $scope.getThreadReplies(response.data.message[0].id);
    });
    $scope.reloadThreads = function(){
        chatFunctions.getThreads(1).then(function(response){
            $scope.threads = response.data.message;
            $scope.noMoreData = false;
            $scope.pageThread = 1;
        });
    }
    $scope.reloadReplies = function(id){
        chatFunctions.getThreadReplies(id).then(function(response){
            $scope.threadReplies = response.data.message;
            $timeout(function(){chatFunctions.slimScrollBottom('.chatRepliesScroll')},0);
        });
    }
    $scope.paginateThreads = function(){
        $scope.pageThread++;
        $scope.loadingThreads = true;
        chatFunctions.getThreads($scope.pageThread).then(function(response){
            $.merge($scope.threads,response.data.message);
            $scope.loadingThreads = false;
            if(response.data.message.length < 10)
                $scope.noMoreData = true;
        });
    }
    $scope.getThreadReplies = function(id){
        chatFunctions.getThreadReplies(id).then(function(response){
            $scope.threadReplies = response.data.message;
            $timeout(function(){chatFunctions.slimScrollBottom('.chatRepliesScroll')},0);
        });
    }
    $scope.reply = function(message){
        if(!message){
            return true;
        }
        $http.post('/api/v2/chat/message',{id:$scope.user.selectedThread.id,message:message}).success(function(){
            $scope.replyValue = "";
            $scope.getThreadReplies($scope.user.selectedThread.id);

        }).error(function(){
            alert('error');
        })
    }
    $scope.uploadcsv = function(file){
        //var file = $scope.myFile;
        //console.log('file is ' + JSON.stringify(file));
        var uploadUrl = "/fileUpload";
        chatFunctions.uploadFile(file,$scope.user.selectedThread.id).then(function(data){
            $scope.reloadReplies($scope.user.selectedThread.id);
        });
    };
    $scope.getContacts = function(){
        chatFunctions.getContacts().then(function(response){
            $scope.contacts = response.data.message;
            //$timeout(function(){chatFunctions.applySlimScroll('.slimContactScroll',300)},0);
        });
    };
    $scope.getContacts();
    $scope.addMessage = function(){
        var postdata = {subject:$scope.thread.subject,message:$scope.thread.message,to:$scope.multipleChecked}
        chatFunctions.addMessage(postdata).then(function(response){
            $scope.thread = {};
            $scope.multipleChecked = [];
            $scope.reloadThreads();
        });    
    }
    $scope.addPeople = function(){
        $http.post('/api/v2/chat/forward/'+$scope.user.selectedThread.id,{forward:$scope.multipleChecked}).success(function(){
            $scope.multipleChecked = [];
        }).error(function(){
            alert('error');
        });
    }
    $scope.markRead = function(){
        $http.put('/api/v2/chat/meta/'+$scope.user.selectedThread.id,{status:'Read'}).success(function(){
        }).error(function(){
            alert('error');
        });
    }
}]);
angular.module('coreApp').factory('chatFunctions',['$http',function($http){
    this.getThreads = function(page){
        var promise = $http.get('/api/v2/chat',{params:{page:page}}).error(function(){alert('error')});
        return promise;
    },
    this.getThreadReplies = function(id){
        var promise = $http.get('/api/v2/chat/'+id).error(function(){alert('error')});
        return promise;
    }
    this.slimScrollBottom = function(element){
        var scrollHeight = $(element).prop('scrollHeight');
        $(element).slimscroll({scrollTo:scrollHeight});
    }
    this.uploadFile = function(file,id){
        var fd = new FormData();
        fd.append('id', id);
        fd.append('attachment', file);
        var promise = $http.post("/api/v2/chat/message", fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .error(function(){
            alert('error');
        });
        return promise;
    }
    this.getContacts = function(){
        var promise = $http.get('/api/v2/contact').error(function(){alert('error')});
        return promise;
    }
    this.addMessage = function(data){
        var promise = $http.post("/api/v2/chat",data).error(function(){
            alert('error');
        });
        return promise;
    }
    return{
        getThreads: this.getThreads,
        getThreadReplies:this.getThreadReplies,
        slimScrollBottom:this.slimScrollBottom,
        uploadFile:this.uploadFile,
        getContacts:this.getContacts,
        addMessage:this.addMessage
    }
}]);
angular.module('coreApp').directive('scrollEndDetect',[function(){
    function link(scope,element,attrs){
        $(element).slimscroll('.chatScroll',636).bind('slimscroll',function(e,pos){
            if(pos == 'bottom' && !scope.noMoreData){
                scope.$apply(attrs.paginateThreads);
            }
        });
    }
    return{
        link:link
    };
}]).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        scope:{uploadcsv:'&callbackFn'},
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function(){
                scope.$apply(function(){
                    console.log(element[0].files[0]);
                    modelSetter(scope, element[0].files[0]);
                });
                scope.uploadcsv({file:element[0].files[0]});
            });
        }
    };
}])
.directive('multipleCheck', [function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            if (scope.multipleChecked.indexOf(scope.contact.id) !== -1) {
                element[0].checked = true;
            }
            element.bind('click',function(){
                var selectedStack = scope.multipleChecked.indexOf(scope.contact.id);
                if(element[0].checked){
                    if (selectedStack === -1) scope.multipleChecked.push(scope.contact.id);
                }else{
                    if (selectedStack !== -1) scope.multipleChecked.splice(selectedStack, 1);
                }
                scope.$apply(scope.selectedStack);
            })
        }
    };
}])
.directive('markThreadRead',[function(){
    return{
        restrict:'A',
        link:function(scope,element,attrs){
            $(element).on('click',function(){
                $(this).find('.inbox-item-author').addClass('author-disabled');
            })
        }
    }
}])