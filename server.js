var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var fs = require('fs');
var path = require('path');
var request = require('request');
var _ = require('underscore-node');
var cons = require('consolidate'); // The `consolidate` adapter module
var multer  = require('multer');
var session = require('express-session');

var appConfigs = require('./app-configuration.json');
var moduleConfigs = require('./module-configuration.json');

var root = fs.realpathSync('.');
var app = express();
app.use(multer({ dest: './uploads/'}))
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
}));
////app.enable('strict routing');

//configuring vendor based middlewares
app.use('/assets', express.static(__dirname + '/assets/'));  //handling the statics - assets (js, css, images)
app.use('/skins', express.static(__dirname + '/skins/'));  //handling the statics - skins (partials)
app.use('/application-modules', express.static(__dirname + '/application-modules/'));  //handling the statics - (controllers, templates etc)

app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended:false}));

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

//rendering engine
app.set('views', './');
app.engine('html', cons.underscore);
app.set('view engine', 'html');

app.set('env', 'dev'); //two settings for environment available: 'prod' or 'dev'

app.set('devAPI', 'http://dev-cerebellum.cloudapp.net');
app.set('prodAPI', 'http://cerebellum.medocity.net');


//CUSTOM MIDDLEWARES
app.use(function (req, res, next) {
    _.each(appConfigs, function (appData) {
        if (appData.URL === req.headers.host) {
            req.appData = appData;
            next();
        }
    });
});

app.get('/resetCaregiver',function(req,res){
    var token = req.cookies.serviceToken;
    var application = req.appData.appName;
    var host = app.get('env') == 'dev' ? app.get('devAPI') : app.get('prodAPI');
    var headers={
        session: token,
        app: application,
        "content-type":req.headers['content-type']
    }
    var method = req.method;
    var uri = host + '/v2/profile/caregiver/stopcaregiving';

    request.put({url: uri,headers:headers}, function (error, httpResponse, body) {
        res.redirect('/platform/patient-dashboard');
    });
});

//intercepting API requests
app.use('/api/*', function (req, res, next) {

    var token = req.cookies.serviceToken;
    var application = req.appData.appName;
    var host = app.get('env') == 'dev' ? app.get('devAPI') : app.get('prodAPI');
    var headers={
        session: token,
        app: application,
        "content-type":req.headers['content-type']
    }
    var urlArr = req.originalUrl.split('/');
    urlArr.splice(0, 2);
    var path = urlArr.join('/');

    var method = req.method;
    var uri = host + '/' + path;

    //POST INTERCEPTOR
    if (method == 'POST') {
        
        if(_.isEmpty(req.files))
        {
            
            request.post({url: uri, json: req.body,headers:headers}, function (error, httpResponse, body) {
                res.status(httpResponse.statusCode).send(body);
            });
        }
        else
        {
            var formData = req.body;
            var fileData = _.pairs(req.files)[0][1];
            formData[fileData.fieldname] = fs.createReadStream(__dirname + '/uploads/'+fileData.name);
            request.post({url: uri, formData: formData, headers:headers}, function (error, httpResponse, body) {
                res.status(httpResponse.statusCode).send(body);
            });
        }
    }
    //PUT INTERCEPTOR
    if (method == 'PUT') {
        if(_.isEmpty(req.files)){
            request.put({url: uri, json: req.body, headers: headers}, function (error, httpResponse, body) {
                res.status(httpResponse.statusCode).send(body);
            });
        }
        else
        {
            var formData = req.body;
            var fileData = _.pairs(req.files)[0][1];
            formData[fileData.fieldname] = fs.createReadStream(__dirname + '/uploads/'+fileData.name);
            request.put({url: uri, formData: formData, headers: headers}, function (error, httpResponse, body) {
                res.status(httpResponse.statusCode).send(body);
            });

        }
    }

    //GET INTERCEPTOR
    if (method == 'GET') {
        request.get({url: uri,headers:headers}, function (error, httpResponse, body) {
            res.status(httpResponse.statusCode).send(JSON.parse(body));
        });
    }

    //DELETE INTERCEPTOR
    if (method == 'DELETE') {
        request.del({url: uri,headers:headers}, function (error, httpResponse, body) {
            res.status(httpResponse.statusCode).send(body);
        });
    }
});

//ROUTES
app.get('/changePassword.html',function(req,res){
    res.sendFile(path.join(root, 'changePassword.html'));
});

app.get('/', function (req, res) {
    res.sendFile(path.join(root, 'skins/' + req.appData.skin + '/partials/index.html'));
});

app.get('/platform/*', ensureAuthenticated, function (req, res) {
    var modules = req.appData.modules;
    //aggregating module configurations from module-configuration.json
    var moduleData = [];  var submods = [];
    _.each(modules, function (module) {
          submods = [];
          var moduleConfig1 = {};
        _.each(moduleConfigs, function (moduleConfig) {
            if (module == moduleConfig.moduleId) {
                moduleConfig1.name = moduleConfig.name;
                moduleConfig1.icon = moduleConfig.icon;
                moduleConfig1.url = moduleConfig.url;
                if(moduleConfig.submodules){
                    //console.log(moduleConfig.submodules);
                    _.each(moduleConfig.submodules,function(sub){

                        _.each(sub.availableIn,function(availability)
                        {
                            if(availability == req.appData.appType)
                            {
                                submods.push(sub);
                            }
                        });
                    });
                }
                moduleConfig1.submodules = submods;
                moduleData.push(moduleConfig1);
            }
        })
    });
    res.render('layout.html', {modules: moduleData, appData: req.appData, caregiver:req.cookies.caregiverName?req.cookies.caregiverName:false});
});


app.get('/logout',function(req,res){
    var token = req.cookies.serviceToken;
    var application = req.appData.appType;
    var host = app.get('env') == 'dev' ? app.get('devAPI') : app.get('prodAPI');
    var headers = {
        session: token,
        app: application
    }
    request.post({url: host + '/v2/logout',headers:headers}, function (error, httpResponse, body) {
       var response = JSON.parse(body);
        if (response.success) {
            res.redirect('/');
        }
        else {
            res.redirect('/');
        }
    });
});

//do not allow any other url apart from the above routes
app.get('/*', function (req, res) {
    res.redirect('/')
});


//UTILITY FUNCTIONS

//validates the session token and redirects appropriately
function ensureAuthenticated(req, res, next) {
    var token = req.cookies.serviceToken;
    var application = req.appData.appType;
    if (req.cookies.serviceToken && req.appData.appType) {
        var host = app.get('env') == 'dev' ? app.get('devAPI') : app.get('prodAPI');
        request.get({url: host + '/v2/validatetoken/' + token}, function (error, httpResponse, body) {
                var response = JSON.parse(body);
                if (response.success) {
                    next();
                }
                else {
                    res.redirect('/');
                }
        });
    }
    else {
        res.redirect('/');
    }
}

//SERVER LISTENING
var port = process.env.PORT || 80;

var server = app.listen(port, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Node server running at http://%s:%s. API in use: %s', host, port, app.get('env'));
});